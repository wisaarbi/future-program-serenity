@SearchingWikipedia
Feature: Searching blibli in duckduckgo

  @blilbi
  Scenario: Search blibli Wikipedia in duckduckgo
    Given user open duckduckgo homepage
    When user type "blibli.com wikipedia" in search box
    And user click wikipedia link
    Then user should able to see "Blibli.com adalah salah satu situs web perdagangan elektronik di Indonesia."

  @mangga
  Scenario: Search mangga Wikipedia in duckduckgo
    Given user open duckduckgo homepage
    When user type "mangga wikipedia" in search box
    And user click wikipedia link
    Then user should able to see "Mangga atau mempelam adalah nama sejenis buah, demikian pula nama pohonnya."