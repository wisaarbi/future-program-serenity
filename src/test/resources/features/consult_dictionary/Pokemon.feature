@VerifyPokemon
Feature: Verify Pokemon

  Scenario Outline: Verify Pokemon <PokemonName> from SerebiiKantoPokedex page to PokemonDBRedBlueYellow page and pokemon api
    Given User open SerebiiKantoPokedex page
    Then User verify that in SerebiiKantoPokedex page contains "<PokemonName>"
    When User click "<PokemonName>" in SerebiiKantoPokedex page
    Then User save the data from SerebiiKantoPokedex page

    Given [api] set pokemon to "<PokemonName>"
    When  [api] send read pokemon request

    Given User open PokemonDBRedBlueYellow page
    Then User verify that in PokemonDBRedBlueYellow page contains "<PokemonName>"
    When User click "<PokemonName>" in PokemonDBRedBlueYellow page
    Then User save the data from PokemonDBRedBlueYellow page
    Then User should able to see same data pokemon from SerebiiKantoPokedex page and PokemonDBRedBlueYellow page and pokemon api

    Examples:
      | PokemonName |
      | Charmeleon  |

