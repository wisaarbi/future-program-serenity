@FillingForm
Feature: Filling Form

  Scenario Outline: User fill form with hosting (<IsHosting>) and user from <State>
    Given User open formpage
    When User fill the form with the following data :
      | firstName          | <FirstName>          |
      | lastName           | <LastName>           |
      | email              | <Email>              |
      | phone              | <Phone>              |
      | address            | <Address>            |
      | city               | <City>               |
      | state              | <State>              |
      | zipCode            | <ZipCode>            |
      | website            | <Website>            |
      | isHosting          | <IsHosting>          |
      | projectDescription | <ProjectDescription> |
    Then User should able to see the url "https://www.seleniumeasy.com/test/input-form-demo.html"

    Examples:
      | FirstName | LastName | Email                   | Phone       | Address                   | City          | State  | ZipCode | Website          | IsHosting | ProjectDescription |
      | Blibli    | Dot Com  | customercare@blibli.com | 08123456789 | Jalan Budi Kemuliaan No 2 | Jakarta Pusat | alaska | 61252   | testselenium.com | true      | E-Commerce         |
      | Blibli    | Dot Com  | customercare@blibli.com | 08123456789 | Jalan Budi Kemuliaan No 2 | Jakarta Pusat | alaska | 61252   | testselenium.com | false     | E-Commerce         |
      | Blibli    | Dot Com  | customercare@blibli.com | 08123456789 | Jalan Budi Kemuliaan No 2 | Jakarta Pusat | hawaii | 61252   | testselenium.com | true      | E-Commerce         |
      | Blibli    | Dot Com  | customercare@blibli.com | 08123456789 | Jalan Budi Kemuliaan No 2 | Jakarta Pusat | hawaii | 61252   | testselenium.com | false      | E-Commerce         |