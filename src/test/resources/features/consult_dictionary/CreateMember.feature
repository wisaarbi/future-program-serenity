@Member @CreateMember
Feature: Create Member

  @PositiveScenario
  Scenario: Create member success
    Given [api] set email to "membersdats@gmail.com"
    And   [api] set name to "membersatu"
    And   [api] set phone number to "0812332066831"
    When  [api] send create member request
    Then  [api] create member status code must be '200'
    And   [api] create member response equals with request

  @NegativeScenario
  Scenario: Create member email is not valid
    Given [api] set email to "membersatuaddgmaildotcom"
    And   [api] set name to "membersatu"
    And   [api] set phone number to "08123066831"
    When  [api] send create member request
    Then  [api] create member status code must be '400'
    And   [api] create member errors should be "email format is invalid"

