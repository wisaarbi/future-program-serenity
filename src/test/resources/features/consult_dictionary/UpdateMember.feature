@Member @UpdateMember
Feature: Update Member

  @PositiveScenario
  Scenario: Update member success
    Given [api] set id to "2"
    And   [api] set email to "membersatqdwqu@gmail.com"
    And   [api] set name to "memberdqwisatu"
    And   [api] set phone number to "08122066831"
    When  [api] send update member request
    Then  [api] update member status code must be '200'
    And   [api] update member response equals with request

    @NegativeScenario
    Scenario: Update member email is not valid
      Given [api] set id to "2"
      And   [api] set email to "membersatqdwqudhqh8cq"
      And   [api] set name to "memberdqwisatu"
      And   [api] set phone number to "08122066831"
      When  [api] send update member request
      Then  [api] update member status code must be '400'
      And   [api] update member errors should be "email format is invalid"