@Member @DeleteMember
Feature: Delete Member

  @PositiveScenario
  Scenario: Delete member success
    Given [api] set id to "5"
    When  [api] send delete member request
    Then  [api] delete member status code must be '200'
    And   [api] delete member response data should be "true"

  @NegativeScenario
  Scenario: Delete member id is not available
    Given [api] set id to "1"
    When  [api] send delete member request
    Then  [api] delete member status code must be '400'
    And   [api] delete member errors should be "id not found"