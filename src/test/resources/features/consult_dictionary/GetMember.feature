@Member @GetMember
Feature: Get Member

  @PositiveScenario
  Scenario: Get member success
    Given [api] set id to "2"
    When  [api] send read member request
    Then  [api] read member status code must be '200'
    And   [api] read member response equals with request

  @NegativeScenario
  Scenario: Get member id is not found
    Given [api] set id to "1"
    When  [api] send read member request
    Then  [api] read member status code must be '400'
    And   [api] read member errors should be "id not found"