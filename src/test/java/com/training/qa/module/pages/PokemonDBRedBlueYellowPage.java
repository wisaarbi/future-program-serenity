package com.training.qa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

@DefaultUrl("https://pokemondb.net/pokedex/game/red-blue-yellow")
public class PokemonDBRedBlueYellowPage extends PageObject {

    @FindBy(xpath = "//main[@class='main-content grid-container']//h1")
    private WebElementFacade pokemonName;

    @FindBy(xpath = "//table[@class='vitals-table']//strong")
    private WebElementFacade pokemonNumber;

    @FindBy(xpath = "(//table[@class='vitals-table']//td)[3]")
    private WebElementFacade pokemonSpecies;

    @FindBy(xpath = "(//table[@class='vitals-table']//td)[4]")
    private WebElementFacade pokemonHeight;

    @FindBy(xpath = "(//table[@class='vitals-table']//td)[5]")
    private WebElementFacade pokemonWeight;

    private static By LIST_POKEMON_NAME = By.xpath("//a[@class='ent-name']");
    private static By LIST_POKEMON_DETAIL_TYPE = By.xpath("(//table[@class='vitals-table']//td)[2]//a");

    public List<String> getPokemonNameList() {
        List<WebElementFacade> webElementFacadeListPokemonName = findAll(LIST_POKEMON_NAME);
        List<String> listPokemonName = new ArrayList<>();
        for (WebElementFacade webElementFacade : webElementFacadeListPokemonName) {
            listPokemonName.add(webElementFacade.getText());
        }
        return listPokemonName;
    }

    public void clickPokemonName(String namePokemon) {
        List<WebElementFacade> webElementFacadeListPokemonName = findAll(LIST_POKEMON_NAME);
        for (WebElementFacade webElementFacade : webElementFacadeListPokemonName) {
            if(webElementFacade.getText().contains(namePokemon)) {
                webElementFacade.click();
                break;
            }
        }
    }

    public String getPokemonName() {
        return pokemonName.getText().trim();
    }

    public String getPokemonNumber() {
        return pokemonNumber.getText().trim();
    }

    public String getPokemonSpecies() {
        return pokemonSpecies.getText().trim();
    }

    public String[] getPokemonType() {
        List<WebElementFacade> webElementFacadeListPokemonDetailType = findAll(LIST_POKEMON_DETAIL_TYPE);
        String[] listPokemonDetailType = new String[webElementFacadeListPokemonDetailType.size()];
        for(int i = 0; i < webElementFacadeListPokemonDetailType.size(); i++) {
            listPokemonDetailType[i] = webElementFacadeListPokemonDetailType.get(i).getText().toUpperCase().trim();
        }
        return listPokemonDetailType;
    }

    public Double getPokemonHeight() {
        String height = pokemonHeight.getText();
        Double heightInMeter = Double.parseDouble(height.substring(0, height.indexOf("m")).trim());
        return heightInMeter;
    }

    public Double getPokemonWeight() {
        String weight = pokemonWeight.getText();
        Double weightInKg = Double.parseDouble(weight.substring(0, weight.indexOf("k")).trim());
        return weightInKg;
    }
}
