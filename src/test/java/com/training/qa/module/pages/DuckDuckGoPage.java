package com.training.qa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://duckduckgo.com/")
public class DuckDuckGoPage extends PageObject {
    @FindBy(name = "q")
    private WebElementFacade txtSearch;

    @FindBy(id = "r1-0")
    private WebElementFacade linkWikipediaBlibli;

    public void searchAndEnter(String keyword) {
        txtSearch.typeAndEnter(keyword);
    }

    public void clickBlibli() {
        linkWikipediaBlibli.click();
    }

}
