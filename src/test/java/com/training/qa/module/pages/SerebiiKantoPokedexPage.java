package com.training.qa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.*;

@DefaultUrl("https://www.serebii.net/fireredleafgreen/kantopokedex.shtml")
public class SerebiiKantoPokedexPage extends PageObject {

    @FindBy(xpath = "(//td[@class='fooinfo'][3]//td[2])[1]")
    private WebElementFacade pokemonNumber;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[1]")
    private WebElementFacade pokemonName;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[5]")
    private WebElementFacade pokemonSpecies;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[6]")
    private WebElementFacade pokemonHeight;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[7]")
    private WebElementFacade pokemonWeight;

    private static By LIST_POKEMON_NAME = By.xpath("(//table[@class='tab'])[2]//td[@class='fooinfo'][3]//a");
    private static By LIST_POKEMON_DETAIL_TYPE = By.xpath("(//td[@class='tooltabcon'])[4]//img");
    public static final Map<String, String> INTERNAL_CONTENT_POKEMON_TYPE;
    static {
        Map<String, String> map = new HashMap<>();
        map.put("https://www.serebii.net/pokedex-bw/type/bug.gif", "BUG");
        map.put("https://www.serebii.net/pokedex-bw/type/poison.gif", "POISON");
        map.put("https://www.serebii.net/pokedex-bw/type/fire.gif", "FIRE");
        map.put("https://www.serebii.net/pokedex-bw/type/flying.gif", "FLYING");
        map.put("https://www.serebii.net/pokedex-bw/type/water.gif", "WATER");
        map.put("https://www.serebii.net/pokedex-bw/type/electric.gif", "ELECTRIC");
        map.put("https://www.serebii.net/pokedex-bw/type/ground.gif", "GROUND");
        map.put("https://www.serebii.net/pokedex-bw/type/grass.gif", "GRASS");
        map.put("https://www.serebii.net/pokedex-bw/type/fighting.gif", "FIGHTING");
        INTERNAL_CONTENT_POKEMON_TYPE = Collections.unmodifiableMap(map);
    }

    public List<String> getPokemonNameList() {
        List<WebElementFacade> webElementFacadeListPokemonName = findAll(LIST_POKEMON_NAME);
        List<String> listPokemonName = new ArrayList<>();
        for (WebElementFacade webElementFacade : webElementFacadeListPokemonName) {
            listPokemonName.add(webElementFacade.getText());
        }
        return listPokemonName;
    }

    public void clickPokemonName(String namePokemon) {
        List<WebElementFacade> webElementFacadeListPokemonName = findAll(LIST_POKEMON_NAME);
        for (WebElementFacade webElementFacade : webElementFacadeListPokemonName) {
            if(webElementFacade.getText().contains(namePokemon)) {
                webElementFacade.click();
                break;
            }
        }
    }

    public String getPokemonName() {
        return pokemonName.getText().trim();
    }

    public String getPokemonNumber() {
        return pokemonNumber.getText().trim().substring(1);
    }

    public String getPokemonSpecies() {
        return pokemonSpecies.getText().trim();
    }

    public String[] getPokemonType() {
        List<WebElementFacade> webElementFacadeListPokemonDetailType = findAll(LIST_POKEMON_DETAIL_TYPE);
        String[] listPokemonDetailType = new String[webElementFacadeListPokemonDetailType.size()];
        for(int i = 0; i < webElementFacadeListPokemonDetailType.size(); i++) {
            listPokemonDetailType[i] = INTERNAL_CONTENT_POKEMON_TYPE
                    .get(webElementFacadeListPokemonDetailType.get(i).getAttribute("src").trim());
        }
        return listPokemonDetailType;
    }

    public Double getPokemonHeight() {
        String height = pokemonHeight.getText();
        Double heightInMeter = Double.parseDouble(height.substring(height.indexOf("”")+1, height.indexOf("m")).trim());
        return heightInMeter;
    }

    public Double getPokemonWeight() {
        String weight = pokemonWeight.getText();
        Double weightInKg = Double.parseDouble(weight.substring(weight.indexOf("s")+1, weight.indexOf("k")).trim());
        return weightInKg;
    }
}
