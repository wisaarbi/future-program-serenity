package com.training.qa.module.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class WikipediaPage extends PageObject {
    @FindBy(id = "mw-content-text")
    WebElementFacade definition;

    public String getDefinition() {
        return definition.getText();
    }
}
