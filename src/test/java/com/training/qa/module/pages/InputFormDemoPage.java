package com.training.qa.module.pages;

import com.sun.javafx.binding.StringFormatter;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.seleniumeasy.com/test/input-form-demo.html")
public class InputFormDemoPage extends PageObject {
    @FindBy(name = "first_name")
    private WebElementFacade txtFirstName;

    @FindBy(name = "last_name")
    private WebElementFacade txtLastName;

    @FindBy(name = "email")
    private WebElementFacade txtEmail;

    @FindBy(name = "phone")
    private WebElementFacade txtPhone;

    @FindBy(name = "address")
    private WebElementFacade txtAddress;

    @FindBy(name = "city")
    private WebElementFacade txtCity;

    @FindBy(name = "state")
    private WebElementFacade selectState;

    @FindBy(name = "zip")
    private WebElementFacade txtZipCode;

    @FindBy(name = "website")
    private WebElementFacade txtWebsite;

    @FindBy(xpath = "//input[@name='hosting'][@value = 'yes']//parent::label")
    private WebElementFacade radioHostingYes;

    @FindBy(xpath = "//input[@name='hosting'][@value = 'no']//parent::label")
    private WebElementFacade radioHostingNo;

    @FindBy(name = "comment")
    private WebElementFacade txtAreaProjectDescription;

    public void fillFirstName(String firstName) {
        txtFirstName.type(firstName);
    }

    public void fillLastName(String lastName) {
        txtLastName.type(lastName);
    }

    public void fillEmail(String email) {
        txtEmail.type(email);
    }

    public void fillPhone(String phone) {
        txtPhone.type(phone);
    }

    public void fillAddress(String address) {
        txtAddress.type(address);
    }

    public void fillCity(String city) {
        txtCity.type(city);
    }

    public void fillState(String state) {
        String words[]=state.split("\\s");
        String stateCapitalizeWord="";
        for(String w:words){
            String first=w.substring(0,1);
            String afterfirst=w.substring(1);
            stateCapitalizeWord+=first.toUpperCase()+afterfirst+" ";
        }
        selectState.selectByVisibleText(stateCapitalizeWord.trim());
    }

    public void fillZipCode(String zipCode) {
        txtZipCode.type(zipCode);
    }

    public void fillWebsite(String webSite) {
        txtWebsite.type(webSite);
    }

    public void fillHosting(Boolean isHosting) {
        if(isHosting) {
            fillHostingYes();
        }
        else {
            fillHostingNo();
        }
    }

    public void fillHostingYes() {
        radioHostingYes.click();
    }

    public void fillHostingNo() {
        radioHostingNo.click();
    }

    public void fillProjectDescription(String projectDescription) {
        txtAreaProjectDescription.type(projectDescription);
    }
}
