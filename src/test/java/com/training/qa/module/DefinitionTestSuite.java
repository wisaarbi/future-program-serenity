package com.training.qa.module;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/consult_dictionary/",
tags = "@VerifyPokemon")
public class DefinitionTestSuite {
}
