package com.training.qa.module.controller;

import com.training.qa.module.data.MemberData;
import com.training.qa.module.data.PokemonData;
import io.restassured.response.Response;

import static net.serenitybdd.rest.SerenityRest.given;

public class PokemonController {
    public static Response readPokemon() {
        return  given().
//                header("Accept", "application/json").
                when().
                get("https://pokeapi.co/api/v2/pokemon/" + PokemonData.getName().trim().toLowerCase());
    }

    public static Response readType(String url) {
        return  given().
                header("Accept", "application/json").
                header("Content-Type", "application/json").
                when().
                get(url);
    }
}
