package com.training.qa.module.controller;

import com.training.qa.module.data.MemberData;
import com.training.qa.module.request.CreateMemberRequest;
import com.training.qa.module.request.UpdateMemberRequest;
import io.restassured.response.Response;

import static net.serenitybdd.rest.SerenityRest.given;

public class MemberController {
    public static Response readMember() {
        return  given().
                queryParam("id", MemberData.getId()).
                header("Accept", "application/json").
                when().
                get("http://localhost:17001/api/member");
    }

    public static Response createMember() {
        CreateMemberRequest request = new CreateMemberRequest();
        request.setEmail(MemberData.getEmail());
        request.setName(MemberData.getName());
        request.setPhoneNumber(MemberData.getPhoneNumber());
        return  given().
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                body(request).
                when().
                post("http://localhost:17001/api/member");
    }

    public static Response updateMember() {
        UpdateMemberRequest request = new UpdateMemberRequest();
        request.setEmail(MemberData.getEmail());
        request.setName(MemberData.getName());
        request.setPhoneNumber(MemberData.getPhoneNumber());

        return  given().
                queryParam("id", MemberData.getId()).
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                body(request).
                when().
                put("http://localhost:17001/api/member");
    }

    public static Response deleteMember () {
        return  given().
                queryParam("id", MemberData.getId()).
                header("Accept", "application/json").
                when().
                delete("http://localhost:17001/api/member");
    }
}
