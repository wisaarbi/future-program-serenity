package com.training.qa.module.response;

public class UpdateMemberResponse {
    private int code;
    private UpdateMemberResponseData data;
    private String status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UpdateMemberResponseData getData() {
        return data;
    }

    public void setData(UpdateMemberResponseData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
