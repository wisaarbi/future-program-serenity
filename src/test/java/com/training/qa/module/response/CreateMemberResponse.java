package com.training.qa.module.response;

public class CreateMemberResponse {
    private int code;
    private CreateMemberResponseData data;
    private String status;
    private String error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public CreateMemberResponseData getData() {
        return data;
    }

    public void setData(CreateMemberResponseData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
