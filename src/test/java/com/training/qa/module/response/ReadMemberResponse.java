package com.training.qa.module.response;

public class ReadMemberResponse {
    private int code;
    private ReadMemberResponseData data;
    private String status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ReadMemberResponseData getData() {
        return data;
    }

    public void setData(ReadMemberResponseData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
