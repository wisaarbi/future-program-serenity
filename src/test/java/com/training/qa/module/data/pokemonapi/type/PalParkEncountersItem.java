package com.training.qa.module.data.pokemonapi.type;

public class PalParkEncountersItem{
	private Area area;
	private int baseScore;
	private int rate;

	public void setArea(Area area){
		this.area = area;
	}

	public Area getArea(){
		return area;
	}

	public void setBaseScore(int baseScore){
		this.baseScore = baseScore;
	}

	public int getBaseScore(){
		return baseScore;
	}

	public void setRate(int rate){
		this.rate = rate;
	}

	public int getRate(){
		return rate;
	}

	@Override
 	public String toString(){
		return 
			"PalParkEncountersItem{" + 
			"area = '" + area + '\'' + 
			",base_score = '" + baseScore + '\'' + 
			",rate = '" + rate + '\'' + 
			"}";
		}
}
