package com.training.qa.module.data.pokemonapi;

public class Sprites{
	private String backShinyFemale;
	private String backFemale;
	private String backDefault;
	private String frontShinyFemale;
	private String frontDefault;
	private String frontFemale;
	private String backShiny;
	private String frontShiny;

	public void setBackShinyFemale(String backShinyFemale){
		this.backShinyFemale = backShinyFemale;
	}

	public String getBackShinyFemale(){
		return backShinyFemale;
	}

	public void setBackFemale(String backFemale){
		this.backFemale = backFemale;
	}

	public String getBackFemale(){
		return backFemale;
	}

	public void setBackDefault(String backDefault){
		this.backDefault = backDefault;
	}

	public String getBackDefault(){
		return backDefault;
	}

	public void setFrontShinyFemale(String frontShinyFemale){
		this.frontShinyFemale = frontShinyFemale;
	}

	public String getFrontShinyFemale(){
		return frontShinyFemale;
	}

	public void setFrontDefault(String frontDefault){
		this.frontDefault = frontDefault;
	}

	public String getFrontDefault(){
		return frontDefault;
	}

	public void setFrontFemale(String frontFemale){
		this.frontFemale = frontFemale;
	}

	public String getFrontFemale(){
		return frontFemale;
	}

	public void setBackShiny(String backShiny){
		this.backShiny = backShiny;
	}

	public String getBackShiny(){
		return backShiny;
	}

	public void setFrontShiny(String frontShiny){
		this.frontShiny = frontShiny;
	}

	public String getFrontShiny(){
		return frontShiny;
	}

	@Override
 	public String toString(){
		return 
			"Sprites{" + 
			"back_shiny_female = '" + backShinyFemale + '\'' + 
			",back_female = '" + backFemale + '\'' + 
			",back_default = '" + backDefault + '\'' + 
			",front_shiny_female = '" + frontShinyFemale + '\'' + 
			",front_default = '" + frontDefault + '\'' + 
			",front_female = '" + frontFemale + '\'' + 
			",back_shiny = '" + backShiny + '\'' + 
			",front_shiny = '" + frontShiny + '\'' + 
			"}";
		}
}
