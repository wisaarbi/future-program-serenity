package com.training.qa.module.data.pokemonapi.type;

public class GeneraItem{
	private String genus;
	private Language language;

	public void setGenus(String genus){
		this.genus = genus;
	}

	public String getGenus(){
		return genus;
	}

	public void setLanguage(Language language){
		this.language = language;
	}

	public Language getLanguage(){
		return language;
	}

	@Override
 	public String toString(){
		return 
			"GeneraItem{" + 
			"genus = '" + genus + '\'' + 
			",language = '" + language + '\'' + 
			"}";
		}
}
