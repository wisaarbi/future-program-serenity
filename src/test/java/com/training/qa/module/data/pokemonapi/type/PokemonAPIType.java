package com.training.qa.module.data.pokemonapi.type;

import java.util.List;

public class PokemonAPIType{
	private EvolutionChain evolutionChain;
	private List<GeneraItem> genera;
	private Habitat habitat;
	private Color color;
	private List<EggGroupsItem> eggGroups;
	private int captureRate;
	private List<PokedexNumbersItem> pokedexNumbers;
	private boolean formsSwitchable;
	private GrowthRate growthRate;
	private List<FlavorTextEntriesItem> flavorTextEntries;
	private int id;
	private boolean isBaby;
	private int order;
	private Generation generation;
	private List<PalParkEncountersItem> palParkEncounters;
	private Shape shape;
	private int baseHappiness;
	private List<NamesItem> names;
	private List<VarietiesItem> varieties;
	private int genderRate;
	private String name;
	private boolean hasGenderDifferences;
	private int hatchCounter;
	private List<Object> formDescriptions;
	private Object evolvesFromSpecies;

	public void setEvolutionChain(EvolutionChain evolutionChain){
		this.evolutionChain = evolutionChain;
	}

	public EvolutionChain getEvolutionChain(){
		return evolutionChain;
	}

	public void setGenera(List<GeneraItem> genera){
		this.genera = genera;
	}

	public List<GeneraItem> getGenera(){
		return genera;
	}

	public void setHabitat(Habitat habitat){
		this.habitat = habitat;
	}

	public Habitat getHabitat(){
		return habitat;
	}

	public void setColor(Color color){
		this.color = color;
	}

	public Color getColor(){
		return color;
	}

	public void setEggGroups(List<EggGroupsItem> eggGroups){
		this.eggGroups = eggGroups;
	}

	public List<EggGroupsItem> getEggGroups(){
		return eggGroups;
	}

	public void setCaptureRate(int captureRate){
		this.captureRate = captureRate;
	}

	public int getCaptureRate(){
		return captureRate;
	}

	public void setPokedexNumbers(List<PokedexNumbersItem> pokedexNumbers){
		this.pokedexNumbers = pokedexNumbers;
	}

	public List<PokedexNumbersItem> getPokedexNumbers(){
		return pokedexNumbers;
	}

	public void setFormsSwitchable(boolean formsSwitchable){
		this.formsSwitchable = formsSwitchable;
	}

	public boolean isFormsSwitchable(){
		return formsSwitchable;
	}

	public void setGrowthRate(GrowthRate growthRate){
		this.growthRate = growthRate;
	}

	public GrowthRate getGrowthRate(){
		return growthRate;
	}

	public void setFlavorTextEntries(List<FlavorTextEntriesItem> flavorTextEntries){
		this.flavorTextEntries = flavorTextEntries;
	}

	public List<FlavorTextEntriesItem> getFlavorTextEntries(){
		return flavorTextEntries;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIsBaby(boolean isBaby){
		this.isBaby = isBaby;
	}

	public boolean isIsBaby(){
		return isBaby;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public int getOrder(){
		return order;
	}

	public void setGeneration(Generation generation){
		this.generation = generation;
	}

	public Generation getGeneration(){
		return generation;
	}

	public void setPalParkEncounters(List<PalParkEncountersItem> palParkEncounters){
		this.palParkEncounters = palParkEncounters;
	}

	public List<PalParkEncountersItem> getPalParkEncounters(){
		return palParkEncounters;
	}

	public void setShape(Shape shape){
		this.shape = shape;
	}

	public Shape getShape(){
		return shape;
	}

	public void setBaseHappiness(int baseHappiness){
		this.baseHappiness = baseHappiness;
	}

	public int getBaseHappiness(){
		return baseHappiness;
	}

	public void setNames(List<NamesItem> names){
		this.names = names;
	}

	public List<NamesItem> getNames(){
		return names;
	}

	public void setVarieties(List<VarietiesItem> varieties){
		this.varieties = varieties;
	}

	public List<VarietiesItem> getVarieties(){
		return varieties;
	}

	public void setGenderRate(int genderRate){
		this.genderRate = genderRate;
	}

	public int getGenderRate(){
		return genderRate;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setHasGenderDifferences(boolean hasGenderDifferences){
		this.hasGenderDifferences = hasGenderDifferences;
	}

	public boolean isHasGenderDifferences(){
		return hasGenderDifferences;
	}

	public void setHatchCounter(int hatchCounter){
		this.hatchCounter = hatchCounter;
	}

	public int getHatchCounter(){
		return hatchCounter;
	}

	public void setFormDescriptions(List<Object> formDescriptions){
		this.formDescriptions = formDescriptions;
	}

	public List<Object> getFormDescriptions(){
		return formDescriptions;
	}

	public void setEvolvesFromSpecies(Object evolvesFromSpecies){
		this.evolvesFromSpecies = evolvesFromSpecies;
	}

	public Object getEvolvesFromSpecies(){
		return evolvesFromSpecies;
	}

	@Override
 	public String toString(){
		return 
			"PokemonAPIType{" + 
			"evolution_chain = '" + evolutionChain + '\'' + 
			",genera = '" + genera + '\'' + 
			",habitat = '" + habitat + '\'' + 
			",color = '" + color + '\'' + 
			",egg_groups = '" + eggGroups + '\'' + 
			",capture_rate = '" + captureRate + '\'' + 
			",pokedex_numbers = '" + pokedexNumbers + '\'' + 
			",forms_switchable = '" + formsSwitchable + '\'' + 
			",growth_rate = '" + growthRate + '\'' + 
			",flavor_text_entries = '" + flavorTextEntries + '\'' + 
			",id = '" + id + '\'' + 
			",is_baby = '" + isBaby + '\'' + 
			",order = '" + order + '\'' + 
			",generation = '" + generation + '\'' + 
			",pal_park_encounters = '" + palParkEncounters + '\'' + 
			",shape = '" + shape + '\'' + 
			",base_happiness = '" + baseHappiness + '\'' + 
			",names = '" + names + '\'' + 
			",varieties = '" + varieties + '\'' + 
			",gender_rate = '" + genderRate + '\'' + 
			",name = '" + name + '\'' + 
			",has_gender_differences = '" + hasGenderDifferences + '\'' + 
			",hatch_counter = '" + hatchCounter + '\'' + 
			",form_descriptions = '" + formDescriptions + '\'' + 
			",evolves_from_species = '" + evolvesFromSpecies + '\'' + 
			"}";
		}
}