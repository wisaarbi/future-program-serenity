package com.training.qa.module.data.pokemonapi.type;

public class FlavorTextEntriesItem{
	private Language language;
	private Version version;
	private String flavorText;

	public void setLanguage(Language language){
		this.language = language;
	}

	public Language getLanguage(){
		return language;
	}

	public void setVersion(Version version){
		this.version = version;
	}

	public Version getVersion(){
		return version;
	}

	public void setFlavorText(String flavorText){
		this.flavorText = flavorText;
	}

	public String getFlavorText(){
		return flavorText;
	}

	@Override
 	public String toString(){
		return 
			"FlavorTextEntriesItem{" + 
			"language = '" + language + '\'' + 
			",version = '" + version + '\'' + 
			",flavor_text = '" + flavorText + '\'' + 
			"}";
		}
}
