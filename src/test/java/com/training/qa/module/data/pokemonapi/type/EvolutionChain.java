package com.training.qa.module.data.pokemonapi.type;

public class EvolutionChain{
	private String url;

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"EvolutionChain{" + 
			"url = '" + url + '\'' + 
			"}";
		}
}
