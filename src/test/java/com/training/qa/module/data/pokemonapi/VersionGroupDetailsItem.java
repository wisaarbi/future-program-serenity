package com.training.qa.module.data.pokemonapi;

public class VersionGroupDetailsItem{
	private int levelLearnedAt;
	private VersionGroup versionGroup;
	private MoveLearnMethod moveLearnMethod;

	public void setLevelLearnedAt(int levelLearnedAt){
		this.levelLearnedAt = levelLearnedAt;
	}

	public int getLevelLearnedAt(){
		return levelLearnedAt;
	}

	public void setVersionGroup(VersionGroup versionGroup){
		this.versionGroup = versionGroup;
	}

	public VersionGroup getVersionGroup(){
		return versionGroup;
	}

	public void setMoveLearnMethod(MoveLearnMethod moveLearnMethod){
		this.moveLearnMethod = moveLearnMethod;
	}

	public MoveLearnMethod getMoveLearnMethod(){
		return moveLearnMethod;
	}

	@Override
 	public String toString(){
		return 
			"VersionGroupDetailsItem{" + 
			"level_learned_at = '" + levelLearnedAt + '\'' + 
			",version_group = '" + versionGroup + '\'' + 
			",move_learn_method = '" + moveLearnMethod + '\'' + 
			"}";
		}
}
