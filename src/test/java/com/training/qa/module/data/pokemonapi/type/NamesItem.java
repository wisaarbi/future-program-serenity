package com.training.qa.module.data.pokemonapi.type;

public class NamesItem{
	private String name;
	private Language language;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLanguage(Language language){
		this.language = language;
	}

	public Language getLanguage(){
		return language;
	}

	@Override
 	public String toString(){
		return 
			"NamesItem{" + 
			"name = '" + name + '\'' + 
			",language = '" + language + '\'' + 
			"}";
		}
}
