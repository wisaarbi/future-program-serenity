package com.training.qa.module.data.pokemonapi.type;

public class PokedexNumbersItem{
	private int entryNumber;
	private Pokedex pokedex;

	public void setEntryNumber(int entryNumber){
		this.entryNumber = entryNumber;
	}

	public int getEntryNumber(){
		return entryNumber;
	}

	public void setPokedex(Pokedex pokedex){
		this.pokedex = pokedex;
	}

	public Pokedex getPokedex(){
		return pokedex;
	}

	@Override
 	public String toString(){
		return 
			"PokedexNumbersItem{" + 
			"entry_number = '" + entryNumber + '\'' + 
			",pokedex = '" + pokedex + '\'' + 
			"}";
		}
}
