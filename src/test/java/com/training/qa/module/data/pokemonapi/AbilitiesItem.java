package com.training.qa.module.data.pokemonapi;

public class AbilitiesItem{
	private boolean isHidden;
	private Ability ability;
	private int slot;

	public void setIsHidden(boolean isHidden){
		this.isHidden = isHidden;
	}

	public boolean isIsHidden(){
		return isHidden;
	}

	public void setAbility(Ability ability){
		this.ability = ability;
	}

	public Ability getAbility(){
		return ability;
	}

	public void setSlot(int slot){
		this.slot = slot;
	}

	public int getSlot(){
		return slot;
	}

	@Override
 	public String toString(){
		return 
			"AbilitiesItem{" + 
			"is_hidden = '" + isHidden + '\'' + 
			",ability = '" + ability + '\'' + 
			",slot = '" + slot + '\'' + 
			"}";
		}
}
