package com.training.qa.module.data;

import io.restassured.response.Response;

public class PokemonData {
    private static String name;
    private static String number;
    private static String species;
    private static String type[];
    private static double height;
    private static double weight;

    private static Response readPokemonResponse;

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        PokemonData.name = name;
    }

    public static String getNumber() {
        return number;
    }

    public static void setNumber(String number) {
        PokemonData.number = number;
    }

    public static String getSpecies() {
        return species;
    }

    public static void setSpecies(String species) {
        PokemonData.species = species;
    }

    public static String[] getType() {
        return type;
    }

    public static void setType(String[] type) {
        PokemonData.type = type;
    }

    public static double getHeight() {
        return height;
    }

    public static void setHeight(double height) {
        PokemonData.height = height;
    }

    public static double getWeight() {
        return weight;
    }

    public static void setWeight(double weight) {
        PokemonData.weight = weight;
    }

    public static Response getReadPokemonResponse() {
        return readPokemonResponse;
    }

    public static void setReadPokemonResponse(Response readPokemonResponse) {
        PokemonData.readPokemonResponse = readPokemonResponse;
    }
}
