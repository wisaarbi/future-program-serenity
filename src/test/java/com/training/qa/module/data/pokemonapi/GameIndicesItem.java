package com.training.qa.module.data.pokemonapi;

public class GameIndicesItem{
	private int gameIndex;
	private Version version;

	public void setGameIndex(int gameIndex){
		this.gameIndex = gameIndex;
	}

	public int getGameIndex(){
		return gameIndex;
	}

	public void setVersion(Version version){
		this.version = version;
	}

	public Version getVersion(){
		return version;
	}

	@Override
 	public String toString(){
		return 
			"GameIndicesItem{" + 
			"game_index = '" + gameIndex + '\'' + 
			",version = '" + version + '\'' + 
			"}";
		}
}
