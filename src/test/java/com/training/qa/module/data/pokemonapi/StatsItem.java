package com.training.qa.module.data.pokemonapi;

public class StatsItem{
	private Stat stat;
	private int baseStat;
	private int effort;

	public void setStat(Stat stat){
		this.stat = stat;
	}

	public Stat getStat(){
		return stat;
	}

	public void setBaseStat(int baseStat){
		this.baseStat = baseStat;
	}

	public int getBaseStat(){
		return baseStat;
	}

	public void setEffort(int effort){
		this.effort = effort;
	}

	public int getEffort(){
		return effort;
	}

	@Override
 	public String toString(){
		return 
			"StatsItem{" + 
			"stat = '" + stat + '\'' + 
			",base_stat = '" + baseStat + '\'' + 
			",effort = '" + effort + '\'' + 
			"}";
		}
}
