package com.training.qa.module.data;

import io.restassured.response.Response;

public class MemberData {
    private static int id;
    private static String email;
    private static String name;
    private static String phoneNumber;

    private static Response readMemberResponse;
    private static Response createMemberResponse;
    private static Response updateMemberResponse;
    private static Response deleteMemberResponse;

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        MemberData.id = id;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        MemberData.email = email;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        MemberData.name = name;
    }

    public static String getPhoneNumber() {
        return phoneNumber;
    }

    public static void setPhoneNumber(String phoneNumber) {
        MemberData.phoneNumber = phoneNumber;
    }

    public static Response getReadMemberResponse() {
        return readMemberResponse;
    }

    public static void setReadMemberResponse(Response readMemberResponse) {
        MemberData.readMemberResponse = readMemberResponse;
    }

    public static Response getCreateMemberResponse() {
        return createMemberResponse;
    }

    public static void setCreateMemberResponse(Response createMemberResponse) {
        MemberData.createMemberResponse = createMemberResponse;
    }

    public static Response getUpdateMemberResponse() {
        return updateMemberResponse;
    }

    public static void setUpdateMemberResponse(Response updateMemberResponse) {
        MemberData.updateMemberResponse = updateMemberResponse;
    }

    public static Response getDeleteMemberResponse() {
        return deleteMemberResponse;
    }

    public static void setDeleteMemberResponse(Response deleteMemberResponse) {
        MemberData.deleteMemberResponse = deleteMemberResponse;
    }
}
