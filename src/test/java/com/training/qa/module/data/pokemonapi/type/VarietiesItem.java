package com.training.qa.module.data.pokemonapi.type;

public class VarietiesItem{
	private Pokemon pokemon;
	private boolean isDefault;

	public void setPokemon(Pokemon pokemon){
		this.pokemon = pokemon;
	}

	public Pokemon getPokemon(){
		return pokemon;
	}

	public void setIsDefault(boolean isDefault){
		this.isDefault = isDefault;
	}

	public boolean isIsDefault(){
		return isDefault;
	}

	@Override
 	public String toString(){
		return 
			"VarietiesItem{" + 
			"pokemon = '" + pokemon + '\'' + 
			",is_default = '" + isDefault + '\'' + 
			"}";
		}
}
