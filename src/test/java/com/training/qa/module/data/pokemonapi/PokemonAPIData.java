package com.training.qa.module.data.pokemonapi;

import java.util.List;

public class PokemonAPIData {
	private String locationAreaEncounters;
	private List<TypesItem> types;
	private int baseExperience;
	private List<HeldItemsItem> heldItems;
	private int weight;
	private boolean isDefault;
	private Sprites sprites;
	private List<AbilitiesItem> abilities;
	private List<GameIndicesItem> gameIndices;
	private Species species;
	private List<StatsItem> stats;
	private List<MovesItem> moves;
	private String name;
	private int id;
	private List<FormsItem> forms;
	private int height;
	private int order;

	public void setLocationAreaEncounters(String locationAreaEncounters){
		this.locationAreaEncounters = locationAreaEncounters;
	}

	public String getLocationAreaEncounters(){
		return locationAreaEncounters;
	}

	public void setTypes(List<TypesItem> types){
		this.types = types;
	}

	public List<TypesItem> getTypes(){
		return types;
	}

	public void setBaseExperience(int baseExperience){
		this.baseExperience = baseExperience;
	}

	public int getBaseExperience(){
		return baseExperience;
	}

	public void setHeldItems(List<HeldItemsItem> heldItems){
		this.heldItems = heldItems;
	}

	public List<HeldItemsItem> getHeldItems(){
		return heldItems;
	}

	public void setWeight(int weight){
		this.weight = weight;
	}

	public int getWeight(){
		return weight;
	}

	public void setIsDefault(boolean isDefault){
		this.isDefault = isDefault;
	}

	public boolean isIsDefault(){
		return isDefault;
	}

	public void setSprites(Sprites sprites){
		this.sprites = sprites;
	}

	public Sprites getSprites(){
		return sprites;
	}

	public void setAbilities(List<AbilitiesItem> abilities){
		this.abilities = abilities;
	}

	public List<AbilitiesItem> getAbilities(){
		return abilities;
	}

	public void setGameIndices(List<GameIndicesItem> gameIndices){
		this.gameIndices = gameIndices;
	}

	public List<GameIndicesItem> getGameIndices(){
		return gameIndices;
	}

	public void setSpecies(Species species){
		this.species = species;
	}

	public Species getSpecies(){
		return species;
	}

	public void setStats(List<StatsItem> stats){
		this.stats = stats;
	}

	public List<StatsItem> getStats(){
		return stats;
	}

	public void setMoves(List<MovesItem> moves){
		this.moves = moves;
	}

	public List<MovesItem> getMoves(){
		return moves;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setForms(List<FormsItem> forms){
		this.forms = forms;
	}

	public List<FormsItem> getForms(){
		return forms;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public int getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"Pokemon{" + 
			"location_area_encounters = '" + locationAreaEncounters + '\'' + 
			",types = '" + types + '\'' + 
			",base_experience = '" + baseExperience + '\'' + 
			",held_items = '" + heldItems + '\'' + 
			",weight = '" + weight + '\'' + 
			",is_default = '" + isDefault + '\'' + 
			",sprites = '" + sprites + '\'' + 
			",abilities = '" + abilities + '\'' + 
			",game_indices = '" + gameIndices + '\'' + 
			",species = '" + species + '\'' + 
			",stats = '" + stats + '\'' + 
			",moves = '" + moves + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",forms = '" + forms + '\'' + 
			",height = '" + height + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}