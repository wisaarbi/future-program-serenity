package com.training.qa.module.steps;

import com.training.qa.module.pages.InputFormDemoPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class FillFormSteps {
    InputFormDemoPage inputFormDemoPage;

    @Given("^User open formpage$")
    public void userOpenFormpage() {
        inputFormDemoPage.open();
    }

    @When("^User fill the form with the following data :$")
    public void userFillTheFormWithTheFollowingData(DataTable dataTable) {
        Map<String, Object> formData = dataTable.asMap(String.class, Object.class);
        inputFormDemoPage.fillFirstName(formData.get("firstName").toString());
        inputFormDemoPage.fillLastName(formData.get("lastName").toString());
        inputFormDemoPage.fillEmail(formData.get("email").toString());
        inputFormDemoPage.fillPhone(formData.get("phone").toString());
        inputFormDemoPage.fillAddress(formData.get("address").toString());
        inputFormDemoPage.fillCity(formData.get("city").toString());
        inputFormDemoPage.fillState(formData.get("state").toString());
        inputFormDemoPage.fillZipCode(formData.get("zipCode").toString());
        inputFormDemoPage.fillWebsite(formData.get("website").toString());
        inputFormDemoPage.fillHosting(Boolean.parseBoolean(formData.get("isHosting").toString()));
        inputFormDemoPage.fillProjectDescription(formData.get("projectDescription").toString());
    }

    @Then("^User should able to see the url \"([^\"]*)\"$")
    public void userShouldAbleToSeeTheUrl(String expectedUrl) {
        assertThat("Invalid URL", "halo", equalTo(expectedUrl));
    }
}
