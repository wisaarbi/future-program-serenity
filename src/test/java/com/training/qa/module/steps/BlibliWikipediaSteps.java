package com.training.qa.module.steps;

import com.training.qa.module.pages.DuckDuckGoPage;
import com.training.qa.module.pages.WikipediaPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class BlibliWikipediaSteps {
    private DuckDuckGoPage duckDuckGoPage;
    private WikipediaPage wikipediaPage;

    @Given("^user open duckduckgo homepage$")
    public void userOpenDuckduckgoHomepage() {
        duckDuckGoPage.open();
    }

    @When("^user type \"([^\"]*)\" in search box$")
    public void userTypeInSearchBox(String keyword) {
        duckDuckGoPage.searchAndEnter(keyword);
    }

    @And("^user click wikipedia link$")
    public void userClickWikipediaLink() {
        duckDuckGoPage.clickBlibli();
    }

    @Then("^user should able to see \"([^\"]*)\"$")
    public void userShouldAbleToSee(String definition) {
        MatcherAssert.assertThat("wikipedia is not opened",
                wikipediaPage.getDefinition(),
                Matchers.containsString(definition));
    }
}
