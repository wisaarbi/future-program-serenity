package com.training.qa.module.steps;

import com.training.qa.module.controller.PokemonController;
import com.training.qa.module.data.PokemonData;
import com.training.qa.module.data.pokemonapi.PokemonAPIData;
import com.training.qa.module.data.pokemonapi.type.PokemonAPIType;
import com.training.qa.module.pages.PokemonDBRedBlueYellowPage;
import com.training.qa.module.pages.SerebiiKantoPokedexPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class VerifyPokemonSteps {
    private SerebiiKantoPokedexPage serebiiKantoPokedexPage;
    private PokemonDBRedBlueYellowPage pokemonDBRedBlueYellowPage;

    @Given("^User open SerebiiKantoPokedex page$")
    public void userOpenSerebiiKantoPokedexPage() {
        serebiiKantoPokedexPage.open();
    }

    @Given("^User open PokemonDBRedBlueYellow page$")
    public void userOpenPokemonDBRedBlueYellowPage() {
        pokemonDBRedBlueYellowPage.open();
    }

    @Then("^User verify that in SerebiiKantoPokedex page contains \"([^\"]*)\"$")
    public void userVerifyThatInSerebiiKantoPokedexPageContains(String namePokemon) {
        assertThat("There is no pokemon in SerebiiKantoPokedex page", serebiiKantoPokedexPage.getPokemonNameList(), hasItems(containsString(namePokemon)));
    }

    @Then("^User verify that in PokemonDBRedBlueYellow page contains \"([^\"]*)\"$")
    public void userVerifyThatInPokemonDBRedBlueYellowPageContains(String namePokemon) {
        assertThat("There is no pokemon in PokemonDBRedBlueYellow page", pokemonDBRedBlueYellowPage.getPokemonNameList(), hasItems(containsString(namePokemon)));
    }

    @When("^User click \"([^\"]*)\" in SerebiiKantoPokedex page$")
    public void userClickInSerebiiKantoPokedexPage(String namePokemon) {
        serebiiKantoPokedexPage.clickPokemonName(namePokemon);
    }

    @When("^User click \"([^\"]*)\" in PokemonDBRedBlueYellow page$")
    public void userClickInPokemonDBRedBlueYellowPage(String namePokemon) {
        pokemonDBRedBlueYellowPage.clickPokemonName(namePokemon);
    }

    @Then("^User save the data from SerebiiKantoPokedex page$")
    public void userSaveTheDataFromSerebiiKantoPokedexPage() {
        PokemonData.setName(serebiiKantoPokedexPage.getPokemonName());
        PokemonData.setNumber(serebiiKantoPokedexPage.getPokemonNumber());
        PokemonData.setSpecies(serebiiKantoPokedexPage.getPokemonSpecies());
        PokemonData.setType(serebiiKantoPokedexPage.getPokemonType());
        PokemonData.setHeight(serebiiKantoPokedexPage.getPokemonHeight());
        PokemonData.setWeight(serebiiKantoPokedexPage.getPokemonWeight());
    }

    @Then("^User save the data from PokemonDBRedBlueYellow page$")
    public void userSaveTheDataFromPokemonDBRedBlueYellowPage() {
        PokemonData.setName(pokemonDBRedBlueYellowPage.getPokemonName());
        PokemonData.setNumber(pokemonDBRedBlueYellowPage.getPokemonNumber());
        PokemonData.setSpecies(pokemonDBRedBlueYellowPage.getPokemonSpecies());
        PokemonData.setType(pokemonDBRedBlueYellowPage.getPokemonType());
        PokemonData.setHeight(pokemonDBRedBlueYellowPage.getPokemonHeight());
        PokemonData.setWeight(pokemonDBRedBlueYellowPage.getPokemonWeight());
    }

    @Given("^\\[api\\] set pokemon to \"([^\"]*)\"$")
    public void apiSetPokemonTo(String pokemon) {
        PokemonData.setName(pokemon);
    }

    @When("^\\[api\\] send read pokemon request$")
    public void apiSendReadPokemonRequest() {
        PokemonData.setReadPokemonResponse(PokemonController.readPokemon());
    }

    @Then("^User should able to see same data pokemon from SerebiiKantoPokedex page and PokemonDBRedBlueYellow page and pokemon api$")
    public void userShouldAbleToSeeSameDataPokemonFromSerebiiKantoPokedexPageAndPokemonDBRedBlueYellowPageAndPokemonApi() {
        PokemonAPIData response = PokemonData.getReadPokemonResponse()
                .getBody().as(PokemonAPIData.class);
        PokemonAPIType responseType = PokemonController.readType(response.getSpecies().getUrl())
                .getBody().as(PokemonAPIType.class);

        assertThat("Pokemon Name Is Not Same", PokemonData.getName(), equalToIgnoringCase(pokemonDBRedBlueYellowPage.getPokemonName()));
        assertThat("Pokemon Name Is Not Same", PokemonData.getName(), equalToIgnoringCase(response.getName()));

        assertThat("Pokemon Number Is Not Same", PokemonData.getNumber(), equalTo(pokemonDBRedBlueYellowPage.getPokemonNumber()));
        assertThat("Pokemon Number Is Not Same", Integer.parseInt(PokemonData.getNumber()), equalTo(response.getId()));

        assertThat("Pokemon Species Is Not Same", PokemonData.getSpecies(), equalToIgnoringCase(pokemonDBRedBlueYellowPage.getPokemonSpecies()));
        assertThat("Pokemon Species Is Not Same", PokemonData.getSpecies(), equalToIgnoringCase(responseType.getGenera().get(2).getGenus()));

        assertThat("Pokemon Type Is Not Same", PokemonData.getType(), arrayContaining(pokemonDBRedBlueYellowPage.getPokemonType()));
        for(int i = 0; i < PokemonData.getType().length; i++) {
            assertThat("Pokemon Type Is Not Same", PokemonData.getType()[i], equalToIgnoringCase(response.getTypes().get(i).getType().getName()));
        }

        assertThat("Pokemon Height Is Not Same", PokemonData.getHeight(), equalTo(pokemonDBRedBlueYellowPage.getPokemonHeight()));
        assertThat("Pokemon Height Is Not Same", PokemonData.getHeight(), equalTo((response.getHeight()/10.0)));

        assertThat("Pokemon Weight Is Not Same", PokemonData.getWeight(), equalTo(pokemonDBRedBlueYellowPage.getPokemonWeight()));
        assertThat("Pokemon Weight Is Not Same", PokemonData.getWeight(), equalTo((response.getWeight()/10.0)));
    }
}
