package com.training.qa.module.steps;

import com.training.qa.module.controller.MemberController;
import com.training.qa.module.data.MemberData;
import com.training.qa.module.response.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MemberSteps {

    @Given("^\\[api\\] set email to \"([^\"]*)\"$")
    public void apiSetEmailTo(String email) {
        MemberData.setEmail(email);
    }

    @And("^\\[api\\] set name to \"([^\"]*)\"$")
    public void apiSetNameTo(String name) {
        MemberData.setName(name);
    }

    @And("^\\[api\\] set phone number to \"([^\"]*)\"$")
    public void apiSetPhoneNumberTo(String phoneNumber) {
        MemberData.setPhoneNumber(phoneNumber);
    }

    @When("^\\[api\\] send create member request$")
    public void apiSendCreateMemberRequest() {
        MemberData.setCreateMemberResponse(MemberController.createMember());
    }

    @Then("^\\[api\\] create member status code must be '(\\d+)'$")
    public void apiCreateMemberStatusCodeMustBe(int statusCode) {
        assertThat(MemberData.getCreateMemberResponse().statusCode(), equalTo(statusCode));
    }

    @And("^\\[api\\] create member response equals with request$")
    public void apiCreateMemberResponseEqualsWithRequest() {
        CreateMemberResponse response = MemberData.getCreateMemberResponse()
                .getBody().as(CreateMemberResponse.class);
        assertThat(response.getData().getEmail(), equalTo(MemberData.getEmail()));
        assertThat(response.getData().getName(), equalTo(MemberData.getName()));
        assertThat(response.getData().getPhoneNumber(), equalTo(MemberData.getPhoneNumber()));
    }

    @And("^\\[api\\] create member errors should be \"([^\"]*)\"$")
    public void apiCreateMemberErrorsShouldBe(String errors) {
        ResponseFail response = MemberData.getCreateMemberResponse()
                .getBody().as(ResponseFail.class);
        assertThat(response.getErrors(), equalTo(errors));
    }

    @Given("^\\[api\\] set id to \"([^\"]*)\"$")
    public void apiSetIdTo(String id) {
        MemberData.setId(Integer.parseInt(id));
    }

    @When("^\\[api\\] send read member request$")
    public void apiSendReadMemberRequest() {
        MemberData.setReadMemberResponse(MemberController.readMember());
    }

    @Then("^\\[api\\] read member status code must be '(\\d+)'$")
    public void apiReadMemberStatusCodeMustBe(int statusCode) {
        assertThat(MemberData.getReadMemberResponse().statusCode(), equalTo(statusCode));
    }

    @And("^\\[api\\] read member response equals with request$")
    public void apiReadMemberResponseEqualsWithRequest() {
        ReadMemberResponse response = MemberData.getReadMemberResponse()
                .getBody().as(ReadMemberResponse.class);
        assertThat(response.getData().getId(), equalTo(MemberData.getId()));
    }

    @And("^\\[api\\] read member errors should be \"([^\"]*)\"$")
    public void apiReadMemberErrorsShouldBe(String errors) {
        ResponseFail response = MemberData.getReadMemberResponse()
                .getBody().as(ResponseFail.class);
        assertThat(response.getErrors(), equalTo(errors));
    }

    @When("^\\[api\\] send update member request$")
    public void apiSendUpdateMemberRequest() {
        MemberData.setUpdateMemberResponse(MemberController.updateMember());
    }

    @Then("^\\[api\\] update member status code must be '(\\d+)'$")
    public void apiUpdateMemberStatusCodeMustBe(int statusCode) {
        assertThat(MemberData.getUpdateMemberResponse().statusCode(), equalTo(statusCode));
    }

    @And("^\\[api\\] update member response equals with request$")
    public void apiUpdateMemberResponseEqualsWithRequest() {
        UpdateMemberResponse response = MemberData.getUpdateMemberResponse()
                .getBody().as(UpdateMemberResponse.class);
        assertThat(response.getData().getId(), equalTo(MemberData.getId()));
        assertThat(response.getData().getEmail(), equalTo(MemberData.getEmail()));
        assertThat(response.getData().getName(), equalTo(MemberData.getName()));
        assertThat(response.getData().getPhoneNumber(), equalTo(MemberData.getPhoneNumber()));
    }

    @And("^\\[api\\] update member errors should be \"([^\"]*)\"$")
    public void apiUpdateMemberErrorsShouldBe(String errors) throws Throwable {
        ResponseFail response = MemberData.getUpdateMemberResponse()
                .getBody().as(ResponseFail.class);
        assertThat(response.getErrors(), equalTo(errors));
    }

    @When("^\\[api\\] send delete member request$")
    public void apiSendDeleteMemberRequest() {
        MemberData.setDeleteMemberResponse(MemberController.deleteMember());
    }

    @Then("^\\[api\\] delete member status code must be '(\\d+)'$")
    public void apiDeleteMemberStatusCodeMustBe(int statusCode) {
        assertThat(MemberData.getDeleteMemberResponse().statusCode(), equalTo(statusCode));
    }

    @And("^\\[api\\] delete member response data should be \"([^\"]*)\"$")
    public void apiDeleteMemberResponseDataShouldBe(String data) {
        DeleteMemberResponse response = MemberData.getDeleteMemberResponse()
                .getBody().as(DeleteMemberResponse.class);
        assertThat(response.isData(), equalTo(Boolean.parseBoolean(data)));
    }

    @And("^\\[api\\] delete member errors should be \"([^\"]*)\"$")
    public void apiDeleteMemberErrorsShouldBe(String errors) {
        ResponseFail response = MemberData.getDeleteMemberResponse()
                .getBody().as(ResponseFail.class);
        assertThat(response.getErrors(), equalTo(errors));
    }
}
